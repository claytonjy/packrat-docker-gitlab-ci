FROM rocker/r-ver:3.4.4

COPY .Rprofile .Rprofile
COPY packrat/init.R packrat/
COPY packrat/packrat.lock packrat/
COPY packrat/packrat.opts packrat/
COPY packrat/src/ packrat/src/

RUN R --vanilla -f packrat/init.R --args --bootstrap-packrat

COPY analysis.R analysis.R

CMD R -f analysis.R
