# Packrat, Docker, & Gitlab CI

This project serves as an example for using Gitlab's built-in CI to create Docker containers with R code whose dependencies are versioned with Packrat.

Easier goals:
- Fully-automated updates via CI
- Reliable dependencies via versioned Rocker images & Packrat
- Quick build times through caching of the `packrat/lib/` directory

Harder goals:
- understand caching in face of dependency updates (will Packrat know to rebuild the binaries?)
- stripped-down images that contain the binaries, but not the source (multi-stage builds?)


## Usage

You should be able to call the `analysis.R` script from within R like

```r
source("analysis.R", echo = TRUE)  # won't print output without echo
```

or from the command-line like

```bash
R -f analysis.R
```
